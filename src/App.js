/*
    Creado desde 0 por Cristian Salvador Orihuela Torres @ Numbers&Colors
*/
import React, {useState, useEffect} from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import { ToastProvider } from 'react-toast-notifications'
//blockchain
import Web3 from "web3"
import Token from "./abis/Token.json"
import Midhas from "./abis/MidhasMultinivel.json"
//services
import * as firebase from 'firebase';
import {saveLocalUser, guardarToken, guardarMidhas, ponerCallback, setAdmin, setSaldoContratoMDH, setSaldoContrato} from "./services/GlobalApp"
//pages
import SignIn from "./pages/Signin/Signin"
import Layout from "./layout/Layout"
//iniciar
firebase.initializeApp({
    apiKey: "AIzaSyBn448F76_dEDfUuaELbIM4Z1kIxacCUs4",
    authDomain: "midhas-8213e.firebaseapp.com",
    databaseURL: "https://midhas-8213e.firebaseio.com",
    projectId: "midhas-8213e",
    storageBucket: "midhas-8213e.appspot.com",
    messagingSenderId: "618512862928",
    appId: "1:618512862928:web:35bda3618124a01b4458d5"
});

export default function App() {
    const [estado, ponerEstado] = useState(0)
    const [registered, setRegistered] = useState(false)
    const obtenerDatosWallet = async (admins) =>{
        let ETH = window.web3.eth
        let auxAccount = {}
        const acc = await ETH.getAccounts()
        auxAccount.wallet = acc[0]
        const balance = await ETH.getBalance(auxAccount.wallet)
        auxAccount.balance = window.web3.utils.fromWei(balance, "Ether")
        
        const netID = await ETH.net.getId()
        if(Token.networks[netID]){
            const token = new ETH.Contract(Token.abi, Token.networks[netID].address)
            guardarToken(token)
            let balanceToken = await token.methods.balanceOf(auxAccount.wallet).call()
            auxAccount.balanceToken = balanceToken ? window.web3.utils.fromWei(balanceToken.toString(), "Ether") : 0
            let balanceContratoMDH = await token.methods.balanceOf(Midhas.networks[netID].address).call()
            balanceContratoMDH= window.web3.utils.fromWei(balanceContratoMDH.toString(), "Ether") 
            //console.log("el balance del contrato", balanceContrato)
            setSaldoContratoMDH(balanceContratoMDH)
            let balanceContrato = await ETH.getBalance(Midhas.networks[netID].address)
            balanceContrato= window.web3.utils.fromWei(balanceContrato.toString(), "Ether") 
            //console.log("el balance del contrato", balanceContrato)
            setSaldoContrato(balanceContrato)
        } else {
            window.alert("El contrato del Token no ha sido compilado para esta red de ETH.")
        }

        if(Midhas.networks[netID]){
            const midhas = new ETH.Contract(Midhas.abi, Midhas.networks[netID].address)
            guardarMidhas(midhas)
            let registrado = await midhas.methods.estaRegistrado(auxAccount.wallet).call()
            console.log("esta registrado", registrado)
            auxAccount.registrado = registrado
            setRegistered(registrado)
            saveLocalUser(auxAccount)
            if(admins.includes(auxAccount.wallet)){
                setAdmin(true)
            }
        } else {
            window.alert("El contrato de Midhas no ha sido compilado para esta red de ETH.")
        }
        
    }
    ponerCallback(obtenerDatosWallet.bind(this))
    const conectarBlockchain = async (admins)=>{
        //ev.preventDefault();
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum);
            try {
                // Request account access if needed
                await window.ethereum.enable();
                obtenerDatosWallet(admins).then(()=>{
                    ponerEstado(1)
                })
            } catch {
                ponerEstado(2)
            }
        }
        // Legacy dapp browsers...
        else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider);
            obtenerDatosWallet(admins).then(()=>{
                ponerEstado(1)
            })
        }
        // Non-dapp browsers...
        else {
            ponerEstado(2)
            console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
        }
    }

    useEffect(()=>{
        firebase.database().ref("administradores").once("value", snap=>{
            let admins = snap.val()
            console.log("los admins", admins)
            conectarBlockchain(admins)
        })
        
    }, [])
    //return (<ToastProvider>{ user ? <Router><Layout/></Router> : <SignIn/> } </ToastProvider>)
    return (estado == 0 ? <div style={{display:"flex", height: 600, justifyContent:"center", alignItems:"center"}}>
        <div style={{textAlign:"center"}}><h1>Esta aplicación requiere acceso a la Blockchain</h1>
        <p>Para interactuar con esta aplicación por favor instala MetaMask. <br/>Cuando estes listo da clic en el botón para conectar la aplicación a tu MetaMask.</p>
        <button className="buttn buttn-primary" onClick={conectarBlockchain}>Conectar con Metamask</button></div>
    </div> : estado == 1 ? <ToastProvider>{ <Router><Layout registered={registered}/></Router> } </ToastProvider> : <div style={{display:"flex", height: 600, justifyContent:"center", alignItems:"center"}}>
        <div style={{textAlign:"center"}}><h1>Conexión denegada</h1>
        <p>Has denegado el acceso a tus wallets mediante MetaMask, por favor concede el acceso para interactuar con esta aplicación.</p>
        <button className="buttn buttn-primary" onClick={conectarBlockchain}>Conectar con Metamask</button></div>
    </div>
    )
}
