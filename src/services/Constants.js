const Constants = {
    production: false,
    configServerURL: ()=>{
        if(Constants.production){
            return 'NOTSET'
        } else {
            return 'http://127.0.0.1:3001'
        }
    },
    platformURL: ()=>{
        if(Constants.production){
            return 'NOTSET'
        } else {
            return 'http://127.0.0.1:3000'
        }
    },
    myDaemonURL: ()=>{
        return 'http://127.0.0.1:3005'
    }
};

export default Constants