const Paquetes = {
    1 : {
        costo: 0.054,
        comisionNuevoIngreso: 0.0135,
        tipo: "Bronce",
        mensualidad: 0.027,
        nivel1: 0.005859000,
        nivel2: 0.004101300,
        nivel3: 0.001757700
    },
    2 : {
        costo: 0.1,
        comisionNuevoIngreso: 0.025,
        tipo: "Plata",
        mensualidad: 0.05,
        nivel1: 0.010850000,
        nivel2: 0.007595000,
        nivel3: 0.003255000
    },
    3 : {
        costo: 0.22,
        comisionNuevoIngreso: 0.055,
        tipo: "Oro",
        mensualidad: 0.11,
        nivel1: 0.023870000,
        nivel2: 0.016709000,
        nivel3: 0.007161000
    },
    4 : {
        costo: 0.86,
        comisionNuevoIngreso: 0.215,
        tipo: "Platino",
        mensualidad: 0.43,
        nivel1: 0.093310000,
        nivel2: 0.065317000,
        nivel3: 0.027993000
    }
}

export default Paquetes