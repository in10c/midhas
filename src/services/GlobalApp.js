let user = null
let token = null
let midhas = null
let callback = null
let admin = false
let saldoContrato = 0
let saldoContratoMDH = 0
export function saveLocalUser(usr){
    user = usr
}
export function getLocalUser(){
    return user
}
export function deleteUser(){
    user = null
}

export function guardarToken(_tkn){
    token = _tkn
}
export function obtenerToken(){
    return token
}

export function guardarMidhas(_midhs){
    midhas = _midhs
}
export function obtenerMidhas(){
    return midhas
}
export function ponerCallback(_cb){
    callback = _cb
}

export function getCallback(){
    return callback
}

export function setAdmin(boole){
    admin = boole
}

export function esAdmin(){
    return admin
}

export function setSaldoContrato(boole){
    saldoContrato = boole
}

export function getSaldoContrato(){
    return saldoContrato
}
export function setSaldoContratoMDH(boole){
    saldoContratoMDH = boole
}

export function getSaldoContratoMDH(){
    return saldoContratoMDH
}