pragma solidity^0.5.0;
import "./Token.sol";

contract MidhasMultinivel {
    string public name = "Midhas Economia Colaborativa";
    Token public token;
    uint public radio = 50;
    //estructuras
    struct Cliente {
        uint256 fechaInscripcion;
        address direccion;
        string tipoCuenta;
    }
    struct Paquete {
        uint256 costo;
        uint256 comisionNuevoIngreso;
        string tipo;
        uint256 mensualidad;
        uint256 nivel1;
        uint256 nivel2;
        uint256 nivel3;
    }
    struct Puntero {
        bool existe;
        bool activa;
        uint256 totalRef;
        int puntero;
        int indexPaquete;
        bool socio;
        uint fechaExpira;
    }
    //el mapa de toda la organizacion
    struct Organizacion {
        mapping(int => Cliente) clientes;
        int totalRegistrados;
    }
    Organizacion Midhas;
    address Dueno;
    address Empresa;
    address SocioA;
    address SocioB;
    address Regalos;
    //direcciones para entrar al cliente y registrar en hijos
    mapping(address => mapping(int => address)) RutasDeCliente;
    mapping(address => Puntero) DatosCli;
    mapping(int => Paquete) TiposPaquete;
    //eventos
    event TokenComprado(
        address cuenta,
        address token,
        uint cantidad,
        string tipoPago,
        string detalle
    );
    event TokenVendido(
        address cuenta,
        address token,
        uint cantidad
    );
    //funciones
    constructor(Token _token, address _walletEmpresa, address _socioA, address _socioB, address _regalos) public {
        token = _token;
        Midhas.totalRegistrados = 1;
        Dueno = msg.sender;
        Empresa = _walletEmpresa;
        SocioA = _socioA;
        SocioB = _socioB;
        Regalos = _regalos;
        TiposPaquete[1] = Paquete({
            costo: 0.054 ether,
            comisionNuevoIngreso: 0.0135 ether,
            tipo: "Bronce",
            mensualidad: 0.027 ether,
            nivel1: 0.005859000 ether,
            nivel2: 0.004101300	ether,
            nivel3: 0.001757700 ether
        });
        TiposPaquete[2] = Paquete({
            costo: 0.1 ether,
            comisionNuevoIngreso: 0.025 ether,
            tipo: "Plata",
            mensualidad: 0.05 ether,
            nivel1: 0.010850000 ether,
            nivel2: 0.007595000 ether,
            nivel3: 0.003255000 ether
        });
        TiposPaquete[3] = Paquete({
            costo: 0.22 ether,
            comisionNuevoIngreso: 0.055 ether,
            tipo: "Oro",
            mensualidad: 0.11 ether,
            nivel1: 0.023870000 ether,
            nivel2: 0.016709000 ether,
            nivel3: 0.007161000 ether
        });
        TiposPaquete[4] = Paquete({
            costo: 0.86 ether,
            comisionNuevoIngreso: 0.215 ether,
            tipo: "Platino",
            mensualidad: 0.43 ether,
            nivel1: 0.093310000 ether,
            nivel2: 0.065317000 ether,
            nivel3: 0.027993000 ether
        });
    }
    function registrarSocio(address _direccion) public{
        require(msg.sender == Dueno, "Solo el dueño de la compañia puede llamar esta función");
        require(!DatosCli[_direccion].existe, "La dirección ya ha sido registrada previamente.");
        Midhas.clientes[Midhas.totalRegistrados] = Cliente({
            fechaInscripcion : now,
            direccion : _direccion,
            tipoCuenta : "Socio"
        });
        registrarPuntero(_direccion, 0, 4, true);
    }
    function registrarCuenta(int _tipoPaquete, address payable _ref) public payable {
        Puntero memory referenc = DatosCli[_ref];
        require(!DatosCli[msg.sender].existe, "La dirección ya ha sido registrada previamente.");
        require(referenc.existe, "La dirección del referenciador no existe.");
        require(referenc.fechaExpira > now || referenc.socio == true, "La cuenta del referenciador ha caducado.");
        require(msg.value == TiposPaquete[_tipoPaquete].costo, "El paquete a comprar no coincide con el monto de ETH enviado.");
        //primero la comision al papa del 25porciento
        uint noPagado = 0;
        uint pagarPapa;
        //si es mayor el paquete comprado al que tiene el referenciador
        if(_tipoPaquete > referenc.indexPaquete){
            pagarPapa = TiposPaquete[referenc.indexPaquete].comisionNuevoIngreso;
            noPagado += (TiposPaquete[_tipoPaquete].comisionNuevoIngreso - pagarPapa);
        //si no es mayor se paga completo
        } else {
            pagarPapa = TiposPaquete[_tipoPaquete].comisionNuevoIngreso;
        }
        (bool success, ) = _ref.call.value(pagarPapa)("");
        require(success, "La transacción hacia el papa no fue realizada");
        //enviamos la comision a la empresa, el mismo monto que al papa 25 porciento
        depositarEmpresa(TiposPaquete[_tipoPaquete].comisionNuevoIngreso, noPagado);
        //se retorna el 50 porciento pagado en eth en MDH coin
        uint montoTokens = (msg.value / 2) * radio;
        //el balance en MDH del contrato inteligente debe ser mayor o igual al requerido
        require(token.balanceOf(address(this)) >= montoTokens, "El monto a comprar supera el balance en MDH del contrato inteligente.");
        token.transfer(msg.sender, montoTokens);
        //emitir q tokens fueron transferidos
        emit TokenComprado(msg.sender, address(token), montoTokens, "inscripcion", TiposPaquete[_tipoPaquete].tipo);
        //register user
        anadirCliente(_tipoPaquete, _ref, msg.sender);
    }
    function depositarEmpresa(uint256 totalDepositar, uint noPagado) internal{
        uint256 comisionEmpresa = (totalDepositar * 10 / 100) + noPagado;
        (bool listoEmpresa, ) = Empresa.call.value(comisionEmpresa)("");
        require(listoEmpresa, "La transacción hacia la empresa no fue realizada");
        //enviamos la comision al socio A y socio B
        uint256 comisionSocios = totalDepositar * 45 / 100;
        (bool listoSocioA, ) = SocioA.call.value(comisionSocios)("");
        require(listoSocioA, "La transacción hacia el socio A no fue realizada");
        (bool listoSocioB, ) = SocioB.call.value(comisionSocios)("");
        require(listoSocioB, "La transacción hacia el socio B no fue realizada");
    }
    function anadirCliente(int _tipoPaquete, address _ref, address _usuario) internal {
        Midhas.clientes[Midhas.totalRegistrados] = Cliente({
            fechaInscripcion : now,
            direccion : _usuario,
            tipoCuenta : TiposPaquete[_tipoPaquete].tipo
        });
        //si no tiene referencias su padre
        RutasDeCliente[_usuario][1] = _ref;
        //referencias con maximo 3 niveles
        if (DatosCli[_ref].totalRef == 0){
            registrarPuntero(_usuario, 1, _tipoPaquete, false);
        } else if(DatosCli[_ref].totalRef == 1){
            RutasDeCliente[_usuario][2] = RutasDeCliente[_ref][1];
            registrarPuntero(_usuario, 2, _tipoPaquete, false);
        } else if(DatosCli[_ref].totalRef > 1){
            RutasDeCliente[_usuario][2] = RutasDeCliente[_ref][1];
            RutasDeCliente[_usuario][3] = RutasDeCliente[_ref][2];
            registrarPuntero(_usuario, 3, _tipoPaquete, false);
        }
    }
    function registrarPuntero(address _registrador, uint256 _totalRef, int _tipoPaquete, bool _socio) internal{
        DatosCli[_registrador] = Puntero({
            existe: true,
            puntero : Midhas.totalRegistrados,
            totalRef : _totalRef,
            activa: true,
            indexPaquete: _tipoPaquete,
            socio: _socio,
            fechaExpira: now + 30 days
        });
        //sumo un registrado
        Midhas.totalRegistrados ++;
    }
    function misDatos() public view returns(uint256 fechaInscripcion, string memory tipoCuenta, uint256 totalReferidos){
        Cliente memory cli = Midhas.clientes[DatosCli[msg.sender].puntero];
        return(cli.fechaInscripcion, cli.tipoCuenta, DatosCli[msg.sender].totalRef);
    }
    function verCliente(address _wallet) public view
        returns(uint256 fechaInscripcion, string memory tipoCuenta, uint256 totalReferidos, uint fechaExpira, int numeroPaquete, bool socio){
        require(msg.sender == Dueno, "Solo el dueño de la compañia puede llamar esta función");
        Puntero memory datos = DatosCli[_wallet];
        Cliente memory cli = Midhas.clientes[datos.puntero];
        return(cli.fechaInscripcion, cli.tipoCuenta, datos.totalRef, datos.fechaExpira, datos.indexPaquete, datos.socio);
    }
    function estaRegistrado(address _cliente) public view returns(bool registrado){
        return DatosCli[_cliente].existe;
    }
    function traerReferenciados() public view returns(address[] memory){
        address[] memory refs = new address[](DatosCli[msg.sender].totalRef);
        for (uint i = 0; i < DatosCli[msg.sender].totalRef; i++) {
            refs[i] = RutasDeCliente[msg.sender][(int256(i) + 1)];
        }
        return(refs);
    }
    function venderTokens(uint _cantidadVender) public payable{
        require(_cantidadVender > 0, "El monto a vender por el usuario debe ser mayor a 0");
        require(token.balanceOf(msg.sender) >= _cantidadVender, "El monto a vender por el usuario excede su cantidad disponible");
        uint cantidadETH = _cantidadVender / radio;

        require(address(this).balance >= cantidadETH, "El monto a vender supera el balance en Ethereum del exchange");

        token.transferFrom(msg.sender, address(this), _cantidadVender);
        msg.sender.transfer(cantidadETH);
        emit TokenVendido(msg.sender, address(token), _cantidadVender);
    }
    function pagarMensualidad() public payable {
        //ver si no es un socio
        //ver si esta en su fecha de corte
        Puntero memory cli = DatosCli[msg.sender];
        require(cli.socio == false, "Los usuarios socios no requieren pago de mensualidad");
        //paquete al que pertenece el cliente que esta pagando
        Paquete memory paq = TiposPaquete[cli.indexPaquete];
        require(msg.value == paq.mensualidad, "La mensualidad a pagar no coincide con el monto de ETH enviado.");
        //procesa la conversion del 38 porciento a MDH
        uint montoTokens = (msg.value * 38 / 100) * radio;
        uint remanente = (msg.value * 62 / 100);
        //el balance en MDH del contrato inteligente debe ser mayor o igual al requerido
        require(token.balanceOf(address(this)) >= montoTokens, "El monto a comprar supera el balance en MDH del contrato inteligente.");
        token.transfer(msg.sender, montoTokens);
        //emitir q tokens fueron transferidos
        emit TokenComprado(msg.sender, address(token), montoTokens, "mensualidad", paq.tipo);
        //repartimos a padres el 70porciento montos fijos dependiente el paquete comprado
        uint noPagado = repartirMensualidadPapas(msg.sender, paq, cli.totalRef, cli.indexPaquete);
        //al final se pagan las comisiones a la empresa
        cobrarComisiones(remanente, cli.indexPaquete, noPagado);
        //le sumamos dias a su fecha de expiracion
        actualizarSuscripcion(msg.sender);
    }
    function actualizarSuscripcion(address _cliente) internal{
        //si todavia queda tiempo
        if(DatosCli[_cliente].fechaExpira > now){
            DatosCli[_cliente].fechaExpira = DatosCli[_cliente].fechaExpira + 30 days;
        //si ya no quedaba tiempo
        } else {
            DatosCli[_cliente].fechaExpira = now + 30 days;
        }
    }
    function repartirMensualidadPapas(address _cuentaCliente, Paquete memory _paq, uint _totalPapas, int256 _indexPaqueteComprado)
        internal returns(uint noCobrado){
        //guardamos la variable cpn lo no cobrado para enviar al final a la empresa
        uint noPagado = 0;
        //debe tener referidos, ya que los socios no pagan mensualidad
        //procesamos el primer referido, que debe de tener
        uint noPagadoN1 = procesarPagoPapa(_cuentaCliente, 1, _indexPaqueteComprado, _paq.nivel1);
        noPagado += noPagadoN1;
        //vemos si tiene un segundo
        if(_totalPapas > 1){
            uint noPagadoN2 = procesarPagoPapa(_cuentaCliente, 2, _indexPaqueteComprado, _paq.nivel2);
            noPagado += noPagadoN2;
        //si no tiene un segundo, el nivel dos se suma al monto no pagado que caera en la empresa
        } else {
            noPagado += _paq.nivel2;
        }
        //si tiene un tercero
        if(_totalPapas > 2){
            uint noPagadoN3 = procesarPagoPapa(_cuentaCliente, 3, _indexPaqueteComprado, _paq.nivel3);
            noPagado += noPagadoN3;
        //si no tiene un tercero, el nivel tres se suma al monto no pagado que caera en la empresa
        } else {
            noPagado += _paq.nivel3;
        }
        return noPagado;
    }
    function procesarPagoPapa(address _cuentaCliente, int nivel, int256 _indexPaqueteComprado, uint _pagarComprado)
        internal returns (uint noPagado){
        uint sinPagar = 0;
        address referenciador = RutasDeCliente[_cuentaCliente][nivel];
        int indexPaqRef = DatosCli[referenciador].indexPaquete;
        //si el nivel del paquete comprado es mas alto que el que tiene el referenciador
        if(_indexPaqueteComprado > indexPaqRef){
            uint costoPaqPapa;
            if(nivel == 1){
                costoPaqPapa = TiposPaquete[indexPaqRef].nivel1;
            }
            if(nivel == 2){
                costoPaqPapa = TiposPaquete[indexPaqRef].nivel2;
            }
            if(nivel == 3){
                costoPaqPapa = TiposPaquete[indexPaqRef].nivel3;
            }
            uint noPagadoPorLimites = _pagarComprado - costoPaqPapa;
            uint noPagadoN1 = pagarReferido(referenciador, costoPaqPapa);
            sinPagar += noPagadoN1 + noPagadoPorLimites;
        //si el paquete es menor o igual se paga todo
        } else {
            uint noPagadoN1 = pagarReferido(referenciador, _pagarComprado);
            sinPagar += noPagadoN1;
        }
        return sinPagar;
    }
    function pagarReferido(address _referenciador, uint _aPagarXNivel) internal returns(uint noCobrado){
        uint noPagado = 0;
        Puntero memory cuentaRef = DatosCli[_referenciador];
        //vemos cuanto toca por pagar
        //si la cuenta del referenciador esta activa, le hacemos su pago
        if(cuentaRef.existe && ( cuentaRef.fechaExpira > now || cuentaRef.socio == true)){
            //limitaciones del paquete
            (bool listoRef, ) = _referenciador.call.value(_aPagarXNivel)("");
            require(listoRef, "El pago al referido tuvo un error");
        //si la cuenta del referenciador no esta activa, el pago se envia a la empresa
        } else {
            noPagado += _aPagarXNivel;
        }
        return noPagado;
    }
    function cobrarComisiones(uint _remanente, int256 _indicePaquete, uint noPagadoEmpresa) internal{
        //si el paquete es mayor a paquete3 se guarda un 10porciento de comision para regalos y 20 empresa
        if(_indicePaquete > 3){
            uint porcentajeEmpresa = (_remanente * 20 / 100);
            uint porcentajeRegalos = (_remanente * 10 / 100);
            (bool listoRegalos, ) = Regalos.call.value(porcentajeRegalos)("");
            require(listoRegalos, "La transacción hacia la empresa no fue realizada");
            depositarEmpresa(porcentajeEmpresa, noPagadoEmpresa);
        //si el paqeuete es no es mayor a paquete3, no hay regalos, deposito del 30porciento a la empresa
        } else {
            uint porcentajeEmpresa = (_remanente * 30 / 100);
            depositarEmpresa(porcentajeEmpresa, noPagadoEmpresa);
        }
    }
    function subirNivel(int _nuevoTipoPaq) public payable {
        Puntero memory datos = DatosCli[msg.sender];
        require(datos.existe, "La cuenta que ejecuta la transacción no ha sido registrada previamente.");
        Paquete memory nuevoPaq = TiposPaquete[_nuevoTipoPaq];
        require(msg.value == nuevoPaq.costo, "El monto enviado de ETH no coincide con el costo de la actualización");
        //actualizar tipo de cuenta
        Midhas.clientes[datos.puntero].tipoCuenta = nuevoPaq.tipo;
        DatosCli[msg.sender].indexPaquete = _nuevoTipoPaq;
        //le sumamos dias a su fecha de expiracion
        actualizarSuscripcion(msg.sender);
        uint mitad = (nuevoPaq.costo / 2);
        //se retorna el 50 porciento pagado en eth en MDH coin
        uint montoTokens = mitad * radio;
        //el balance en MDH del contrato inteligente debe ser mayor o igual al requerido
        require(token.balanceOf(address(this)) >= montoTokens, "El monto a comprar supera el balance en MDH del contrato inteligente.");
        token.transfer(msg.sender, montoTokens);
        //emitir q tokens fueron transferidos
        emit TokenComprado(msg.sender, address(token), montoTokens, "upgrade", TiposPaquete[_nuevoTipoPaq].tipo);
        depositarEmpresa(mitad, 0);
    }
}