pragma solidity^0.5.0;

import "./Token.sol";

contract EthSwap {
    string public name = "EthSwap Instant Exchange";
    Token public token;
    uint public rate = 5;

    event TokenPurchased(
        address acount,
        address token,
        uint amount,
        uint rate
    );

    event TokenSold(
        address acount,
        address token,
        uint amount,
        uint rate
    );

    constructor(Token _token) public {
        token = _token;
    }

    function buyTokens() public payable{
        uint tokenAmount = msg.value / rate;
        //el balance debe ser mayor o igual a la compra
        require(token.balanceOf(address(this)) >= tokenAmount, "El monto a comprar supera el balance del exchange");
        token.transfer(msg.sender, tokenAmount);
        //emitir q tokens fueron transferidos
        emit TokenPurchased(msg.sender, address(token), tokenAmount, rate);
    }

    function sellTokens(uint _amount) public{
        require(token.balanceOf(msg.sender) >= _amount, "El monto a vender por el usuario excede la cantidad disponible");
        uint usdtAmount = _amount * rate;

        require(address(this).balance >= usdtAmount, "El monto a vender supera el balance del exchange");

        token.transferFrom(msg.sender, address(this), _amount);
        msg.sender.transfer(usdtAmount);
        emit TokenSold(msg.sender, address(token), _amount, rate);
    }
}