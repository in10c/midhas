import React from 'react';
import {Switch, Route, Redirect } from "react-router-dom";
//import layout
import SideBar from "./SideBar"
import TopBar from "./TopBar"
//import pages
import Dashboard from "../pages/Dashboard/Dashboard"
import NuevaCuenta from "../pages/NuevaCuenta/NuevaCuenta"
import Venta from "../pages/Venta/Venta"
import RegistrarSocio from "../pages/RegistrarSocio/RegistrarSocio"
import Familia from "../pages/Familia/Familia"
import LinkReferido from "../pages/LinkReferido/LinkReferido"
import Pagos from "../pages/Pagos/Pagos"
import Transacciones from "../pages/Transacciones/Transacciones"
import Mensualidad from "../pages/Mensualidad/Mensualidad"
import Mejorar from "../pages/Mejorar/Mejorar"
import VerCliente from "../pages/VerCliente/VerCliente"
import Caducar from "../pages/Caducar/Caducar"
import {esAdmin} from "../services/GlobalApp"
//import style
import "./Dashboard.css"

export default function Layout({registered}) {
    let esAdministrador= esAdmin()
    return(
        <div id="mainGrid">
            <SideBar/>
            <div id="mainContent">
                <TopBar/>
                <div id="contentWrapper">
                    {registered && <Switch>
                        <Route exact path="/" component={Dashboard} />
                        <Route exact path="/venta" component={Venta} />
                        <Route exact path="/familia" component={Familia} />
                        <Route exact path="/pagos" component={Pagos} />
                        <Route exact path="/referido" component={LinkReferido} />
                        <Route exact path="/transacciones" component={Transacciones} />
                        <Route exact path="/mensualidad" component={Mensualidad} />
                        <Route exact path="/mejorar" component={Mejorar} />
                        
                    </Switch>}
                    {!registered && !esAdministrador && <Switch>
                        <Route exact path="/nuevacuenta/:wallet?" component={NuevaCuenta} />
                        <Redirect to="/nuevacuenta/" />
                    </Switch>}
                    {!registered && esAdministrador && <Switch>
                        <Route exact path="/familia" component={Familia} />
                        <Route exact path="/pagos" component={Pagos} />
                        <Route exact path="/transacciones" component={Transacciones} />
                        <Route exact path="/registrarSocio" component={RegistrarSocio} />
                        <Route exact path="/vercliente" component={VerCliente} />
                        <Route exact path="/caducar" component={Caducar} />
                    </Switch>}
                </div>
            </div>
        </div>
    )

}