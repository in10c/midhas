import React, {useEffect, Fragment, useState} from 'react';
import { NavLink, useLocation } from "react-router-dom";
import * as firebase from 'firebase';
import {getLocalUser, esAdmin} from "../services/GlobalApp"

export default function SideBar() {
    const [user, setUser ] = useState(null)
    let location = useLocation();
    useEffect(()=>{
        firebase.database().ref("usuarios").child(getLocalUser().wallet).on("value", snap=>{
            let ussr = snap.val()
            console.log("el usuario es", ussr)
            setUser(ussr)
        })
    }, [])
    return(
        <div id="sideBar">
            <div className="logotype">
                <img src="/images/logo.png"/>
                {user && <div style={{marginTop: 15, textAlign:"center"}}>
                    <span style={{fontSize: 20, color: "white", fontWeight: "bold"}}>{user.nombre}</span> <br/>
                    <span style={{fontSize: 17, color: "#e4bc12", fontWeight: "bold"}}>{user.tipoCuenta}</span>
                </div>}
                {esAdmin() &&  <div style={{marginTop: 15, textAlign:"center"}}>
                    <span style={{fontSize: 20, color: "white", fontWeight: "bold"}}>Administración</span>  <br/>
                    <span style={{fontSize: 17, color: "#ff7575", fontWeight: "bold"}}>Superpoderes</span>
                </div>}
                {!esAdmin() && !user &&  <div style={{marginTop: 15, textAlign:"center"}}>
                    <span style={{fontSize: 20, color: "white", fontWeight: "bold"}}>Invitado</span>
                </div>}
            </div>
            
            <div className="menu">
                <ul>
                    {getLocalUser().registrado ? <Fragment>
                        <li className={location.pathname == "/" ? "active" : ""}>
                            <NavLink to="/"><span>Inicio</span></NavLink>
                        </li>
                        <li className={location.pathname == "/pagos" ? "active" : ""}>
                            <NavLink to="/pagos"><span>Pagos</span></NavLink>
                        </li>
                        <li className={location.pathname == "/transacciones" ? "active" : ""}>
                            <NavLink to="/transacciones"><span>Transacciones</span></NavLink>
                        </li>
                        <li className={location.pathname == "/familia" ? "active" : ""}>
                            <NavLink to="/familia"><span>Mi familia</span></NavLink>
                        </li>
                        <li className={location.pathname == "/referido" ? "active" : ""}>
                            <NavLink to="/referido"><span>Enlace de referido</span></NavLink>
                        </li>
                        <li className={location.pathname == "/mensualidad" ? "active" : ""}>
                            <NavLink to="/mensualidad"><span>Pagar mensualidad</span></NavLink>
                        </li>
                        <li className={location.pathname == "/venta" ? "active" : ""}>
                            <NavLink to="/venta"><span>Venta de Tokens</span></NavLink>
                        </li>
                        <li className={location.pathname == "/mejorar" ? "active" : ""}>
                            <NavLink to="/mejorar"><span>Mejorar cuenta</span></NavLink>
                        </li>
                        </Fragment> :  !esAdmin() ?
                        <li className={"active"}>
                            <NavLink to="/"><span>Registrar cuenta</span></NavLink>
                        </li> : null
                    }
                    {
                        esAdmin() && <Fragment>
                        <li className={location.pathname == "/pagos" ? "active" : ""}>
                            <NavLink to="/pagos"><span>Pagos</span></NavLink>
                        </li>
                        <li className={location.pathname == "/transacciones" ? "active" : ""}>
                            <NavLink to="/transacciones"><span>Transacciones</span></NavLink>
                        </li>
                        <li className={location.pathname == "/familia" ? "active" : ""}>
                            <NavLink to="/familia"><span>Mi familia</span></NavLink>
                        </li>
                            <li className={location.pathname == "/vercliente" ? "active" : ""}>
                            <NavLink to="/vercliente"><span>Ver usuario</span></NavLink>
                        </li>
                        <li className={location.pathname == "/caducar" ? "active" : ""}>
                            <NavLink to="/caducar"><span>Caducar cuenta</span></NavLink>
                        </li>
                            <li className={location.pathname == "/registrarSocio" ? "active" : ""}>
                                <NavLink to="/registrarSocio"><span>Registro de socios</span></NavLink>
                            </li>
                        </Fragment>
                    }
                </ul>
            </div>
        </div>
    )

}