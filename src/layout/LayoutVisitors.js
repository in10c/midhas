import React from "react"
import "./LayoutVisitors.css"

export default function LayoutVisitors({children}) {
    
    return <div id="LayoutVisitors">
        <div className="side">
            <img src="/images/white_logotype.png"/>
        </div>
        <div className="content">
            <div className="wrappBox">
                <div className="box">{children}</div>
            </div>
            <div className="legal">
                <span>Términos de uso.</span> <span>Política de privacidad</span>
            </div>
        </div>
    </div>
}