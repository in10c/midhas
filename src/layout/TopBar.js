import React, {Fragment} from 'react';
import {getLocalUser, esAdmin, getSaldoContrato, getSaldoContratoMDH} from "../services/GlobalApp"
import Identicon from "identicon.js"

export default function TopBar() {
    
    return(
        <div id="topBar">
            <div></div>
            {getLocalUser().registrado || esAdmin() ? <div className="saldos">
                <span>MI MDH: {getLocalUser().balanceToken}</span>
                <span>MI ETH: {getLocalUser().balance}</span>
                {esAdmin() && <span> MDH Contrato Inteligente: {getSaldoContratoMDH()}</span>}
                {esAdmin() && <span> ETH Contrato Inteligente: {getSaldoContrato()}</span>}
            </div> : null}
        </div>
    )

}