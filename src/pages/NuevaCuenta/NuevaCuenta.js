import React, {useState, Fragment} from 'react';
import {Redirect} from "react-router-dom";
import axios from "axios"
import Loader from 'react-loader-spinner'
import {  useToasts } from 'react-toast-notifications'
import {obtenerMidhas, getLocalUser, getCallback} from "../../services/GlobalApp"
import paquetes from "../../services/Paquetes"
import "./NuevaCuenta.css"
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

const MiLoader = ()=><div style={{justifyContent: "center", alignItems: "center", display: "flex"}}>
    <Loader
        type="ThreeDots"
        color="#8a7300"
        height={100}
        width={100}

    />
</div>

export default function NuevaCuenta({ match }) {
    const { addToast } = useToasts()
    let { wallet } = match.params
    const [paquete, setPaquete] = useState(4)
    const [nombre, setNombre] = useState("")
    const [loading, setLoading] = useState(false)
    const [terminado, setTerminado] = useState(false)

    const comprar = ()=>{
        setLoading(true)
        //console.log("pagara", paquetes[indice].costo)
        let walletCliente = getLocalUser().wallet
        let today = new Date()
        let time = today.getTime()
        today.setDate( today.getDate() + 30 )
        let fechaExpira = today.getTime()
        let payload = {
            from: walletCliente,
            value: window.web3.utils.toWei(paquetes[paquete].costo+"", "Ether")
        }
        //console.log("el payload es ", payload, (indice+1), wallet)
        obtenerMidhas().methods.registrarCuenta(paquete, wallet).send(payload).on("transactionHash", hash=>{
            //console.log("se cumplio y dio el hash", hash)
            //registramos el usaurio en la base
            let usuario = {
                txID: hash,
                wallet: walletCliente,
                fechaRegistro : time,
                nombre,
                tipoCuenta: paquetes[paquete].tipo,
                indexPaqueteComprado : parseInt(paquete),
                fechaExpira
            }
            axios.post("https://us-central1-midhas-8213e.cloudfunctions.net/crearCliente", {usuario, walletCliente, walletPapa : wallet} ).then(resp=>{
                //console.log("el result es ", resp)
                addToast('El embajador ha sido guardado correctamente, recargue la página en 5 minutos para que se realize la transacción en la cadena de bloques.', { appearance: 'success' })
                setPaquete(3)
                setNombre("")
                setLoading(false)
                setTerminado(true)
            })
        }).on("error", err=>{
            console.log("dio error", err)
            setLoading(false)
        })
    }
    return(
        <div id="registro">
            <h1>Crear nueva cuenta de embajador</h1>
            {wallet ? <div className="box">
                {!loading ? (!terminado ? <Fragment><div>
                    <label>Nombre completo:</label>
                    <input type="text" value={nombre} onChange={ev=>setNombre(ev.target.value)}  />
                </div>
                <div className="paquetes">
                    {Object.keys(paquetes).map((indice)=>{
                        let val = paquetes[indice]
                        return <div className={paquete ==indice ? "activo": ""} onClick={ev=>setPaquete(indice)}>
                            <label>Paquete {val.tipo}</label>
                            <div style={{marginLeft: 10}}><img src={"/images/"+val.tipo+".png"} width="120"/></div>
                            <ul>
                                <li>Costo: {val.costo} ETH</li>
                                <li>Nuevos ingresos: {val.comisionNuevoIngreso} ETH</li>
                                <li>Mensualidad: {val.mensualidad} ETH</li>
                                <li>Mensualidad nivel 1: {val.nivel1} ETH</li>
                                <li>Mensualidad nivel 2: {val.nivel2} ETH</li>
                                <li>Mensualidad nivel 3: {val.nivel3} ETH</li>
                            </ul>
                            
                        </div>
                    })}
                </div>
                <div className="buttons">
                    <button className="buttn buttn-primary" onClick={ev=>comprar()}>Comprar</button>
                </div></Fragment> : 
                <p style={{marginTop:-10}}>Su cuenta ha sido registrada, por favor espere a que MetaMask le indique que la transacción fue realizada y recargue esta página.</p>
            ): <MiLoader />}
            </div>: <div className="box" style={{paddingTop: 0}}>
                <p>Se requiere un id válido de referenciador para ser miembro de Midhas...</p>
            </div>}
        </div> 
    )

}