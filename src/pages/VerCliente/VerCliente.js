import React, {useState} from "react"
import * as firebase from 'firebase';
import axios from "axios"
import {  useToasts } from 'react-toast-notifications'
import {getLocalUser, obtenerMidhas} from "../../services/GlobalApp"
import { regresaFechaConFormato} from "../../services/Fechas"

export default function VerUsuario(){
    const { addToast } = useToasts()
    const [socio, setSocio] = useState("")
    const [data, setData] = useState(false)
    const registrarSocio = async ()=>{
        let registrado = await obtenerMidhas().methods.verCliente(socio).call()
        //let obj = JSON.parse(JSON.stringify(registrado))
        console.log("nos regresa", registrado)
        setData({
            fechaExpira:parseInt(registrado.fechaExpira._hex),
            fechaRegistro: parseInt(registrado.fechaInscripcion._hex),
            nombre: registrado.nombre,
            wallet: registrado.wallet,
            tipoCuenta: registrado.tipoCuenta
        })
    }
    return <div>
        <h1>Info socio</h1>
        <div className="box">
            <div>
                <label>Dirección del nuevo cliente:</label>
                <input type="text" value={socio} onChange={ev=>setSocio(ev.target.value)}  />
            </div>
            {data && <div>
                <ul>
                    <li>Nombre: {data.nombre}</li>
                    <li>Wallet: {data.wallet}</li>
                    <li>Fecha inscripción: {regresaFechaConFormato(data.fechaRegistro)}</li>
                    <li>Fecha Expira: {regresaFechaConFormato(data.fechaExpira)}</li>
                    <li>Time Expira: {(data.fechaExpira)}</li>
                    <li>Paquete comprado: {data.tipoCuenta}</li>
                </ul>
            </div>}
            <div className="buttons">
                <button className="buttn buttn-primary" onClick={registrarSocio}>Registrar</button>
            </div>
        </div>
    </div>
}