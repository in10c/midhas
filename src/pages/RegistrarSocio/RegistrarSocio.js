import React, {useState} from "react"
import * as firebase from 'firebase';
import axios from "axios"
import {  useToasts } from 'react-toast-notifications'
import {getLocalUser, obtenerMidhas} from "../../services/GlobalApp"

export default function RegistrarSocio(){
    const { addToast } = useToasts()
    const [socio, setSocio] = useState("")
    const [nombre, setNombre] = useState("")
    const registrarSocio = ()=>{
        let wallet = getLocalUser().wallet
        let today = new Date()
        obtenerMidhas().methods.registrarSocio(socio).send({
            from: wallet
        }).on("transactionHash", hash=>{
            //console.log("se cumplio y dio el hashsocio registrado")
            let usuario = {
                txID: hash,
                wallet: socio,
                fechaRegistro : today.getTime(),
                nombre,
                fechaExpira: today.getTime(),
                tipoCuenta: "Socio",
                indexPaqueteComprado: 4
            }
            axios.post("https://us-central1-midhas-8213e.cloudfunctions.net/crearSocio", {usuario, socio} ).then(resp=>{
                console.log("el result es ", resp)
            })
        }).on("error", err=>{
            //console.log("dio error", err)
        })
    }
    return <div>
        <h1>Registrar nuevo socio</h1>
        <div className="box">
            <div>
                <label>Nombre del nuevo socio:</label>
                <input type="text" value={nombre} onChange={ev=>setNombre(ev.target.value)}  />
            </div>
            <div>
                <label>Dirección del nuevo socio:</label>
                <input type="text" value={socio} onChange={ev=>setSocio(ev.target.value)}  />
            </div>
            <div className="buttons">
                <button className="buttn buttn-primary" onClick={registrarSocio}>Registrar</button>
            </div>
        </div>
    </div>
}