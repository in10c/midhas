import React, {useState, useEffect} from 'react';
import axios from "axios"
import * as firebase from 'firebase';
import paquetes from "../../services/Paquetes"
import {getLocalUser, obtenerMidhas} from "../../services/GlobalApp"
import "./Mejorar.css"
export default function Mejorar() {
    const [form, setForm] = useState({mdh: 0, eth: 0})
    const [user, setUser ] = useState(null)
    const [paquete, setPaquete] = useState(null)

    useEffect(()=>{
        firebase.database().ref("usuarios").child(getLocalUser().wallet).on("value", snap=>{
            let ussr = snap.val()
            console.log("el usuario es", ussr)
            setUser(ussr)
        })
    }, [])
    const hacerMejora = ()=>{
        if(!paquete){
            alert("Es necesario elegir un paquete")
            return false;
        }
        let payload = {
            from: getLocalUser().wallet,
            value: window.web3.utils.toWei(paquetes[paquete].costo+"", "Ether")
        }
        obtenerMidhas().methods.subirNivel(paquete).send(payload).on("transactionHash", hash=>{
            console.log("se cumplio y dio el hash", hash)
            //registramos el usaurio en la base
            axios.post("https://us-central1-midhas-8213e.cloudfunctions.net/subirNivel", {usuario: user, nuevoPaquete: paquete} ).then(resp=>{
                console.log("el result es ", resp)
              
            })
        }).on("error", err=>{
            console.log("dio error", err)
        })
    }
    const elegirPaquete = (indice)=>{
        if(indice < user.indexPaqueteComprado){
            alert("No se puede elegir este paquete porque es menor al activo.")
            return false
        }
        if(indice == user.indexPaqueteComprado){
            alert("No se puede elegir este paquete porque es igual al activo.")
            return false
        }
        setPaquete(indice)
    }
    return(
        <div id="mejorar">
            <h1>Mejorar cuenta</h1>
            <div className="box">
                <div className="paquetes">
                    {user && Object.keys(paquetes).map((indice)=>{
                        console.log("comparandoo", indice , user.indexPaqueteComprado)
                        let val = paquetes[indice]
                        return <div style={{color: indice > user.indexPaqueteComprado  ? "black":"#d4d4d4" }} className={paquete ==indice ? "activo": ""} onClick={ev=>elegirPaquete(indice)}>
                            <label>Paquete {val.nombre}</label>
                            <img src={"/images/"+val.tipo+".png"} width="200"/>
                            <ul>
                                <li>Costo: {val.costo} ETH</li>
                                
                            </ul>
                            
                        </div>
                    })}
                </div>
                <div className="buttons">
                    <button className="buttn buttn-primary" onClick={hacerMejora}>Mejorar</button>
                </div>
            </div>
        </div>
    )

}