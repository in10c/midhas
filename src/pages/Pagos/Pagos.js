import React, {useEffect, useState} from "react"
import * as firebase from 'firebase';
import { esAdmin, getLocalUser} from "../../services/GlobalApp"
import { regresaFechaConFormatoDesdeTiempo} from "../../services/Fechas"

export default function Pagos() {
    const [listaPagos, setPagos] = useState({})
    useEffect(()=>{
        let aux = esAdmin() ? firebase.database().ref("pagos") : firebase.database().ref("pagosporcliente").child(getLocalUser().wallet)
        aux.on("value", snap=>{
            let pag = snap.val()
            if(pag){
                setPagos(pag)
            }
        })
    }, [])
    return <div>
        <h1>Pagos realizados</h1>
        <div className="box" style={{paddingBottom: 20}}>
            <table>
                <thead>
                    <tr>
                        {esAdmin() && <th>Trans.</th> }
                        <th>Concepto</th>
                        <th>Desde</th>
                        <th>Para</th>
                        <th>Finalizo en</th>
                        <th>Monto</th>
                        <th>Fecha</th>
                        <th>Notas</th>
                    </tr>
                </thead>
                <tbody>
                    {Object.keys(listaPagos).map(index=>{
                    let pago = listaPagos[index]
                    return <tr>
                        {esAdmin() && <td>{pago.transaccionID.substring(pago.transaccionID.length - 5, pago.transaccionID.length)}</td> }
                        <td>{pago.concepto}</td>
                        <td>{pago.desde && pago.desde.nombre} </td>
                        <td>{pago.para && pago.para.nombre} </td>
                        <td>{pago.finalizoen && pago.finalizoen.nombre} </td>
                        <td>{pago.monto} {pago.moneda}</td>
                        <td>{regresaFechaConFormatoDesdeTiempo(pago.fechaRegistro)}</td>
                        <td>{pago.detalles}</td>
                    </tr>})}
                </tbody>
            </table>
        </div>
    </div>
}