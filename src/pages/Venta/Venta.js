import React, {useState, useEffect} from 'react';
import axios from "axios"
import * as firebase from 'firebase';

import {getLocalUser, obtenerMidhas, obtenerToken} from "../../services/GlobalApp"

export default function Venta() {
    const [form, setForm] = useState({mdh: 0, eth: 0})
    const [user, setUser ] = useState(null)
    let radio = 50
    const cambioMDH = (ev)=>{
        setForm({mdh: ev.target.value, eth: fixedToPattern(ev.target.value / radio)})
    }
    const fixedToPattern = (number, pattern) =>{
        let multiplierString = "10000000"
        let multiplier = parseInt(multiplierString)
        return parseInt('' + (number * multiplier)) / multiplier;
    }
    const vender = (ev) =>{
        if(window.confirm("¿Está seguro que desea vender "+form.mdh+" MDH por "+form.eth+" ETH?")){
            let today = new Date()
            let cantidad = window.web3.utils.toWei(form.mdh+"", "Ether")
            let wallet = getLocalUser().wallet
            let midhas = obtenerMidhas()
            obtenerToken().methods.approve(midhas.options.address, cantidad).send({
                from: wallet
            }).on("transactionHash", hash=>{
                console.log("se cumplio y dio el hash, vendamoslos", hash)
                midhas.methods.venderTokens(cantidad).send({
                    from: wallet
                }).on("transactionHash", hash=>{
                    console.log("se cumplio y dio el hash", hash)
                    let usuario = {
                        txID: hash,
                        wallet,
                        fechaRegistro : today.getTime(),
                        nombre: user.nombre,
                    }
                    axios.post("https://us-central1-midhas-8213e.cloudfunctions.net/ventaTokens", {usuario, cantidad: form.mdh} ).then(resp=>{
                        console.log("el result es ", resp)
                    })
                }).on("error", err=>{
                    console.log("dio error", err)
                })
            }).on("error", err=>{
                console.log("dio error", err)
            })
            
        }
    }
    useEffect(()=>{
        firebase.database().ref("usuarios").child(getLocalUser().wallet).on("value", snap=>{
            let ussr = snap.val()
            console.log("el usuario es", ussr)
            setUser(ussr)
        })
    }, [])
    return(
        <div>
            <h1>Vender Tokens MDH</h1>
            <div className="box">
                <div>
                    <label>Cantidad de MDH a vender:</label>
                    <input type="number" value={form.mdh} onChange={cambioMDH} step="0.1" min="0.1" max={getLocalUser().balanceToken} />
                    <span className="notes">Máximo a vender: {getLocalUser().balanceToken} MDH</span>
                </div>
                <div>
                    <label>Cantidad de ETH a obtener:</label>
                    <input type="number" value={form.eth} disabled/>
                </div>
                <div className="buttons">
                    <button className="buttn buttn-primary" onClick={vender}>Vender</button>
                </div>
            </div>
        </div>
    )

}