import React, {useState} from 'react';
import {  useToasts } from 'react-toast-notifications'
import Constants from "../../services/Constants"
import {getLocalUser} from "../../services/GlobalApp"

export default function NuevaCuenta({ match }) {
    const { addToast } = useToasts()
    const link = "https://midhas.finance/nuevaCuenta/"+getLocalUser().wallet
    const copiar = ()=>{
        navigator.clipboard.writeText(link).then(function() {
            addToast('El enlace se ha copiado correctamente al portapapeles.', { appearance: 'success' })
        })
    }
    return(
        <div id="registro">
            <h1>Enlace de referido</h1>
            <div className="box">
                <div>
                    <label>Utiliza el siguiente enlace para poder registrar referidos:</label>
                    <p><a href={link} target="_blank">{link}</a></p>
                </div>
                <div className="buttons">
                    <button className="buttn buttn-primary" onClick={copiar}>Copiar enlace</button>
                </div>
            </div>
        </div>
    )

}