import React, {useState} from "react"
import * as firebase from 'firebase';
import axios from "axios"
import {  useToasts } from 'react-toast-notifications'
import {getLocalUser, obtenerMidhas} from "../../services/GlobalApp"

export default function Caducar(){
    const { addToast } = useToasts()
    const [socio, setSocio] = useState("")
    const [nombre, setNombre] = useState("")
    const registrarSocio = ()=>{
        let wallet = getLocalUser().wallet
        let today = new Date()
        today.setDate(today.getDate()-30)
        obtenerMidhas().methods.caducarCuenta(socio).send({
            from: wallet
        }).on("transactionHash", hash=>{
            console.log("se cumplio y dio el hashsocio registrado")
            firebase.database().ref("usuarios").child(socio).update({
                fechaExpira : today.getTime()
            })
        }).on("error", err=>{
            //console.log("dio error", err)
        })
    }
    return <div>
        <h1>Caducar cuenta socio</h1>
        <div className="box">
            <div>
                <label>Wallet del socio:</label>
                <input type="text" value={socio} onChange={ev=>setSocio(ev.target.value)}  />
            </div>
            <div className="buttons">
                <button className="buttn buttn-primary" onClick={registrarSocio}>Registrar</button>
            </div>
        </div>
    </div>
}