import React, {useState, useCallback, useEffect } from "react"
import * as firebase from 'firebase';
import {  useToasts } from 'react-toast-notifications'
import Hijo from "./Hijo"
import {getLocalUser, esAdmin} from "../../services/GlobalApp"
import "./Familia.css"

export default function Familia(){
    const { addToast } = useToasts()
    const [familia, setFamilia] = useState(null)
    
    useEffect(()=>{
        
        if(esAdmin()){
            firebase.database().ref("arbol").on("value", snapHijos=>{

                setFamilia({
                    nombre: "Empresa",
                    tipoCuenta: "Empresa",
                    hijos: snapHijos.val(),
                    wallet: getLocalUser().wallet
                })
            })
        } else {
            let myWallet = getLocalUser().wallet
            firebase.database().ref("rutas/").child(myWallet).once("value", snap=>{
                let rutas = snap.val()
                if(rutas.ruta === "raiz"){
                    firebase.database().ref("arbol").child(myWallet).on("value", snapHijos=>{
                        console.log("los hijos son ", snapHijos.val())
                        
                        setFamilia(snapHijos.val())
                    })
                } else if(Array.isArray(rutas.ruta)) {
                    let subdirectorio= ""
                    rutas.ruta.map(val=>{
                        subdirectorio+= val+"/hijos/"
                    })
                    firebase.database().ref("arbol/"+subdirectorio).child(myWallet).on("value", snapHijos=>{
                        console.log("los hijos son ", snapHijos.val())
                        
                        setFamilia(snapHijos.val())
                    })
                }
            })
        }
        
    }, [])
    return <div>
        <h1>Registrar nuevo socio</h1>
        <div className="box" id="referidos">
            <ul>
            {familia && <Hijo data={familia} nivel={0} />}
            </ul>
        </div>
    </div>
}