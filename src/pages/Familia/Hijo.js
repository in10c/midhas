import React, {useState} from "react"
import Identicon from "identicon.js"

export default function Hijo({data, nivel}){
    const [visible, setVisible] = useState(false)
    const keys = data.hijos ? Object.keys(data.hijos) : null
    const renderHijos = () =>{
        if(data.hijos){
            return keys.map(index=> <Hijo data={data.hijos[index]} nivel={nivel+1} /> )
        } else {
            return null
        }
    }
    return  <li style={{marginLeft: (50*nivel)}}>
        <div className="content">
            <div className="nivel">{nivel+1}</div>
            <img width="25" height="25" src={"data:image/png;base64,"+(new Identicon(data.wallet, 30).toString())} />
            {data.nombre} ({data.tipoCuenta})
            <div className="numeroHijos">
                {data.hijos ? keys.length : 0}
            </div>
            {data.hijos && keys.length > 0 && <div className="toggle" onClick={ev=>setVisible(!visible)}>
                { !visible ? <i class="fa fa-chevron-down" aria-hidden="true"></i> :
                <i class="fa fa-chevron-up" aria-hidden="true"></i>}
            </div>}    
        </div>
        
        {visible && <ul >
            {renderHijos()}
        </ul>}
    </li> 
}