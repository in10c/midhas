import React, {useState} from "react";
import { useToasts } from 'react-toast-notifications'
//import * as md5 from "md5";
import * as GlobalApp from '../../services/GlobalApp'
import Requests from "../../services/Requests"
import LayoutVisitors from "../../layout/LayoutVisitors"

export default function SignIn()  {
    const [password, setPass] = useState();
    const { addToast } = useToasts()

    const handleSubmit = (event) => {
        event.preventDefault()
        // Requests.post('checkLocalPassword', { password: md5(password)})
        // .then( (response) =>{
        //     console.log("respuesta de check", response)
        //     if(response.data.success == 1){
        //         GlobalApp.saveLocalUser({license: response.data.license})
        //         GlobalApp.executeCallback()
        //     } else {
        //         addToast(response.data.message, { appearance: 'error', autoDismiss: false, pauseOnHover: true })
        //     }
        // })
    }
    return (
        <LayoutVisitors>
            <form onSubmit={handleSubmit} id="formSignIn">
                <h1>Bitwabi</h1>
                <p className="desc">¡Bienvenido!. Identificate para acceder a tu cuenta.</p>
                <div>
                    <div>
                        <input type="password" placeholder="Contraseña única" className="formControl" required name="password"
                            value={password} onChange={ev=>setPass(ev.target.value)}/>
                    </div>
                </div>
                <div>
                    <button className="buttn buttn-primary">Acceder</button>
                </div>
            </form>
        </LayoutVisitors>
    )
}
