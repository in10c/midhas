import React, {useEffect, useState} from "react"
import * as firebase from 'firebase';
import { esAdmin, getLocalUser} from "../../services/GlobalApp"
import { regresaFechaConFormatoDesdeTiempo} from "../../services/Fechas"
import "./Transacciones.css"

export default function Transacciones() {
    const [listaPagos, setPagos] = useState({})
    useEffect(()=>{
        let aux = esAdmin() ? firebase.database().ref("transacciones") : firebase.database().ref("transaccionesporcliente").child(getLocalUser().wallet)
        aux.on("value", snap=>{
            let pag = snap.val()
            if(pag){
                setPagos(pag)
            }
        })
    }, [])
    return <div>
        <h1>Transacciones realizados</h1>
        <div className="box"  style={{paddingBottom: 20}}>
            <table>
                <thead>
                    <tr>
                        <th>Evento</th>
                        <th>Embajador</th>
                        <th>Tipo cuenta</th>
                        <th>Referenciador</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <tbody>
                    {Object.keys(listaPagos).map(index=>{
                    let pago = listaPagos[index]
                    return <tr>
                        <td>{pago.tipo}</td>
                        
                        <td>{pago.embajador.nombre} </td>
                        <td>{pago.tipo == "nuevoIngreso" ? pago.embajador.tipoCuenta : "N/A"}</td>
                        <td>{pago.papa && pago.papa.nombre} </td>
                        <td>{regresaFechaConFormatoDesdeTiempo(pago.fecha)}</td>
                    </tr>})}
                </tbody>
            </table>
        </div>
    </div>
}