import React, {useEffect, useState, Fragment} from "react"
import * as firebase from 'firebase';
import axios from "axios"
import {getLocalUser, obtenerMidhas} from "../../services/GlobalApp"
import { regresaFechaConFormatoDesdeTiempo} from "../../services/Fechas"
import Paquetes from "../../services/Paquetes"

export default function Mensualidad() {
    const [user, setUser ] = useState(null)
    const pagar = ()=>{
        //console.log("pagara", paquetes[indice].costo)
        let walletCliente = getLocalUser().wallet
        let payload = {
            from: walletCliente,
            value: window.web3.utils.toWei(Paquetes[user.indexPaqueteComprado].mensualidad+"", "Ether")
        }
        console.log("el payload es ", payload)
        obtenerMidhas().methods.pagarMensualidad().send(payload).on("transactionHash", hash=>{
            console.log("se cumplio y dio el hash", hash)
            axios.post("https://us-central1-midhas-8213e.cloudfunctions.net/pagarMensualidad", {usuario: user} ).then(resp=>{
                console.log("el result es ", resp)
            })
        }).on("error", err=>{
            console.log("dio error", err)
        })
    }
    useEffect(()=>{
        firebase.database().ref("usuarios").child(getLocalUser().wallet).on("value", snap=>{
            let ussr = snap.val()
            console.log("el usuario es", ussr)
            setUser(ussr)
        })
    }, [])
    return <div>
        <h1>Mensualidad</h1>
        {user && user.tipoCuenta != "Socio" && <div className="box"><div >
            Su mensualidad a pagar es de: {Paquetes[user.indexPaqueteComprado].mensualidad} ETH <br/>
            Su paquete activo es: {Paquetes[user.indexPaqueteComprado].tipo}<br/>
            La fecha en que expira su paquete es: {regresaFechaConFormatoDesdeTiempo(user.fechaExpira)}<br/>
        </div><div>
            <button className="buttn buttn-primary" onClick={pagar}>Pagar mensualidad</button>
        </div></div>}
        {user && user.tipoCuenta == "Socio" && <div className="box" style={{height: 40}}>
            Los socios no requieren pago de mensualidad.<br/>
            </div>}
    </div>
}