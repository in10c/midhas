require("dotenv").config()
require('babel-register');
require('babel-polyfill');
const HDWalletProvider = require("@truffle/hdwallet-provider"); 

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*" // Match any network id
    },
    mainnet: {
        // must be a thunk, otherwise truffle commands may hang in CI
        provider: () => new HDWalletProvider(process.env.MNEMONIC, "https://mainnet.infura.io/v3/"+process.env.INFURA_API_KEY),
        network_id: '1'
    }
  },
  contracts_directory: './src/contracts/',
  contracts_build_directory: './src/abis/',
  compilers: {
    solc: {
      optimizer: {
        enabled: true,
        runs: 200
      },
      evmVersion: "petersburg"
    }
  }
}
