const Token = artifacts.require("Token");
//const EthSwap = artifacts.require("EthSwap");
const Midhas = artifacts.require("MidhasMultinivel")

module.exports = async function(deployer) {
    //deploy token
    await deployer.deploy(Token);
    const token = await Token.deployed()
    let walletEmpresa = "0x3e0A51F2518baa397B1e7Da1ACE54ED3D2793341"
    let walletSocioA = "0x14B03CF57F00924D913C51d7B0cA61576E5EB426"
    let walletSocioB = "0xf5F818ef54610aD828879052AA2b06D8A1B1B69f"
    let walletRegalos = "0x3841a68Fc26c72A40FA0BfDb2788487F7b6A6595"
    //dev
    // let walletEmpresa = "0x654d53035B9A32Efe78c7d0FB4F27bE425A57367"
    // let walletSocioA = "0x89aB8bD3BB395512B8A80B38d6Fe0b0776f5a2E1"
    // let walletSocioB = "0x7C623ee6180111d082C59467CD348cecCB26C848"
    // let walletRegalos = "0xf82fe4e7ae0cef5Ea8E6327306208f6BD4081832"
    //deplot eth
    await deployer.deploy(Midhas, token.address, walletEmpresa, walletSocioA, walletSocioB, walletRegalos);
    const midhas = await Midhas.deployed()

    //transfer all tokens to ETHSWAP
    await token.transfer(midhas.address, "10000000000000000000000000")
};
