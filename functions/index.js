const functions = require('firebase-functions');
const cors = require('cors')({origin: true});
const admin = require('firebase-admin');
admin.initializeApp();
const Paquetes = {
    1 : {
        costo: 0.054,
        comisionNuevoIngreso: 0.0135,
        tipo: "Bronce",
        mensualidad: 0.027,
        nivel1: 0.005859000,
        nivel2: 0.004101300,
        nivel3: 0.001757700
    },
    2 : {
        costo: 0.1,
        comisionNuevoIngreso: 0.025,
        tipo: "Plata",
        mensualidad: 0.05,
        nivel1: 0.010850000,
        nivel2: 0.007595000,
        nivel3: 0.003255000
    },
    3 : {
        costo: 0.22,
        comisionNuevoIngreso: 0.055,
        tipo: "Oro",
        mensualidad: 0.11,
        nivel1: 0.023870000,
        nivel2: 0.016709000,
        nivel3: 0.007161000
    },
    4 : {
        costo: 0.86,
        comisionNuevoIngreso: 0.215,
        tipo: "Platino",
        mensualidad: 0.43,
        nivel1: 0.093310000,
        nivel2: 0.065317000,
        nivel3: 0.027993000
    }
}

//crear cuenta cliente
exports.crearCliente = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        let { usuario, walletCliente, walletPapa } = request.body
        console.log("llegaron params", usuario, walletCliente, walletPapa)
        //registramos usuario
        guardarCliente(usuario, walletCliente, walletPapa).then(()=>{
            registrarTransaccionesNuevoCliente(usuario, walletPapa, walletCliente).then(()=>{
                response.send({success:1})
            }, err=>{
                response.send({success:0})
            })
        }, err=>{
            response.send({success:0})
        })
    })
})

exports.crearSocio = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        let { usuario, socio } = request.body
        let today = new Date()
        await admin.database().ref("usuarios").child(socio).set(usuario)
        await admin.database().ref("arbol").child(socio).set(usuario)
        await admin.database().ref("rutas").child(socio).set({ ruta: "raiz"})
        let transaccion = {
            embajador : {
                nombre: usuario.nombre,
                tipoCuenta: usuario.tipoCuenta,
                wallet: usuario.wallet
            },
            fecha: today.getTime(),
            tipo: "nuevoSocio"
        }
        await admin.database().ref("transacciones").push(transaccion)
        await admin.database().ref("transaccionesporcliente/").child(usuario.wallet).push(transaccion)
        response.send({success:1})
}   )
})

exports.pagarMensualidad = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        let { usuario } = request.body
        let today = new Date()
        let paqueteCliente = Paquetes[usuario.indexPaqueteComprado]
        let montoTokens = (paqueteCliente.mensualidad * 38 / 100) * 50;
        let remanente = (paqueteCliente.mensualidad * 62 / 100);

        //agregar el pago de ingreso MDH
        let transaccion = {
            embajador : {
                nombre: usuario.nombre,
                wallet: usuario.wallet
            },
            fecha: today.getTime(),
            tipo: "pagoMensualidad",
            pagos : []
        }

        let pagoEmbajador = {
            desde: usuario,
            finalizoen: usuario,
            monto : montoTokens,
            concepto: "Tokens comprados por concepto de renovación mensual",
            detalles: null,
            moneda: "MDH",
            fechaRegistro: today.getTime()
        }
        await admin.database().ref("pagosporcliente/").child(usuario.wallet).push(pagoEmbajador)
        transaccion.pagos.push(pagoEmbajador)
        //agregamos el pago a los papas
        let papas = await repartirMensualidadPapas(transaccion, paqueteCliente, usuario)
        await cobrarComision(transaccion,remanente, usuario )
        let trans = await admin.database().ref("transacciones/").push(transaccion)
        transaccion.pagos.map(async (val)=>{
            val.transaccionID= trans.key
            await admin.database().ref("pagos/").push(val)
        })
        delete transaccion.pagos
        await admin.database().ref("transaccionesporcliente/").child(usuario.wallet).push(transaccion)
        //registramos la transacciones en los papas
        console.log("registramos la transacciones en los papas")
        papas.map(async papa=>await admin.database().ref("transaccionesporcliente/").child(papa).push(transaccion))
        let nuevaFechaExpira = new Date(usuario.fechaExpira)
        if(usuario.fechaExpira > today.getTime()){
            nuevaFechaExpira.setDate(nuevaFechaExpira.getDate()+30)
        } else {
            nuevaFechaExpira = new Date()
            nuevaFechaExpira.setDate(today.getDate()+30)
        }

        await admin.database().ref("usuarios").child(usuario.wallet).update({
            fechaExpira: nuevaFechaExpira.getTime()
        })
        response.send({success:1})
}   )
})

exports.ventaTokens = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        let today = new Date()
        let {usuario, cantidad} = request.body
        let transaccion = {
            embajador : {
                nombre: usuario.nombre,
                wallet: usuario.wallet
            },
            fecha: today.getTime(),
            tipo: "ventaTokens",
            pagos : []
        }
        transaccion.pagos.push({
            desde: usuario,
            finalizoen: usuario,
            monto : cantidad * -1,
            concepto: "Venta de MDH",
            detalles: null,
            moneda: "MDH",
            fechaRegistro: today.getTime()
        })
        transaccion.pagos.push({
            desde: usuario,
            finalizoen: usuario,
            monto : cantidad / 50 ,
            concepto: "Compra de ETH",
            detalles: null,
            moneda: "ETH",
            fechaRegistro: today.getTime()
        })
        let trans = await admin.database().ref("transacciones/").push(transaccion)
        await admin.database().ref("transaccionesporcliente/").child(usuario.wallet).push(transaccion)
        transaccion.pagos.map(async (val)=>{
            val.transaccionID= trans.key
            await admin.database().ref("pagos/").push(val)
            await admin.database().ref("pagosporcliente/").child(usuario.wallet).push(val)
        })
        response.send({success:1})
        
    })
})

exports.subirNivel = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        let today = new Date()
        
        let {usuario, nuevoPaquete} = request.body
        let mitad = Paquetes[nuevoPaquete].costo / 2
        let transaccion = {
            embajador : usuario,
            fecha: today.getTime(),
            tipo: "mejoraCuenta",
            nuevoTipoCuenta: Paquetes[nuevoPaquete].tipo,
            pagos : []
        }
        let compraEmbajador = {
            desde: usuario,
            finalizoen: usuario,
            monto : mitad * 50,
            concepto: "Compra de MDH por mejora de cuenta",
            detalles: null,
            moneda: "MDH",
            fechaRegistro: today.getTime()
        }
        transaccion.pagos.push(compraEmbajador)
        pagarEmpresa(mitad, usuario, transaccion, "Pago por mejora de paquete")
        await admin.database().ref("pagosporcliente/").child(usuario.wallet).push(compraEmbajador)
        let trans = await admin.database().ref("transacciones/").push(transaccion)
        transaccion.pagos.map(async (val)=>{
            val.transaccionID= trans.key
            await admin.database().ref("pagos/").push(val)
        })
        delete transaccion.pagos
        await admin.database().ref("transaccionesporcliente/").child(usuario.wallet).push(transaccion)
        
        let nuevaFechaExpira = new Date(usuario.fechaExpira)
        if(usuario.fechaExpira > today.getTime()){
            nuevaFechaExpira.setDate(nuevaFechaExpira.getDate()+30)
        } else {
            nuevaFechaExpira = new Date()
            nuevaFechaExpira.setDate(today.getDate()+30)
        }
        await admin.database().ref("usuarios").child(usuario.wallet).update({
            indexPaqueteComprado: nuevoPaquete,
            tipoCuenta : Paquetes[nuevoPaquete].tipo,
            fechaExpira: nuevaFechaExpira.getTime()
        })
        response.send({success:1})
        
    })
})

function registrarTransaccionesNuevoCliente(usuario, walletPapa, walletCliente){
    return new Promise((resolve, reject)=>{
        let today = new Date()
        let indexHijo = usuario.indexPaqueteComprado
        admin.database().ref("usuarios").child(walletPapa).once("value", async (snap)=>{
            let papa = snap.val()
            let transaccion = {
                embajador : {
                    nombre: usuario.nombre,
                    tipoCuenta: usuario.tipoCuenta,
                    wallet: walletCliente
                },
                papa: {
                    nombre: papa.nombre,
                    tipoCuenta: papa.tipoCuenta,
                    wallet: papa.wallet
                },
                fecha: today.getTime(),
                tipo: "nuevoIngreso",
                pagos : []
            }
            //primero se paga la comision del 25% al papa, si hay sobrantes se van a la empresa
            if(indexHijo > papa.indexPaqueteComprado){
                sobranteEmpresa = Paquetes[indexHijo].comisionNuevoIngreso - Paquetes[papa.indexPaqueteComprado].comisionNuevoIngreso
                //pago papa
                let pagoPapa = {
                    desde: usuario,
                    para: papa,
                    finalizoen: papa,
                    monto : Paquetes[papa.indexPaqueteComprado].comisionNuevoIngreso,
                    concepto: "Comisión referenciador por nuevo ingreso",
                    detalles: "Limitado por nivel de paquete del referenciador, ganancias perdidas "+sobranteEmpresa+" ETH",
                    moneda: "ETH",
                    fechaRegistro: today.getTime()
                }
                transaccion.pagos.push(pagoPapa)
                await admin.database().ref("pagosporcliente/").child(walletPapa).push(pagoPapa)
                
                transaccion.pagos.push({
                    desde: usuario,
                    para: papa,
                    finalizoen: {
                        nombre: "Empresa",
                        wallet : ""
                    },
                    monto : sobranteEmpresa,
                    concepto: "Residuo por pago limitado en comisión por nuevo registro",
                    detalles: null,
                    moneda: "ETH",
                    fechaRegistro: today.getTime()
                })
            } else {
                let pagoPapa = {
                    desde: usuario,
                    para: papa,
                    finalizoen: papa,
                    monto : Paquetes[indexHijo].comisionNuevoIngreso,
                    concepto: "Comisión referenciador por nuevo ingreso",
                    detalles: null,
                    moneda: "ETH",
                    fechaRegistro: today.getTime()
                }
                transaccion.pagos.push(pagoPapa)
                await admin.database().ref("pagosporcliente/").child(walletPapa).push(pagoPapa)
            }
            //ahora se paga 50% en MDH
            let pagoEmbajador = {
                desde: usuario,
                finalizoen: usuario,
                monto : (Paquetes[indexHijo].costo / 2) * 50,
                concepto: "Tokens comprados por ingreso a la plataforma.",
                detalles: null,
                moneda: "MDH",
                fechaRegistro: today.getTime()
            }
            await admin.database().ref("pagosporcliente/").child(walletCliente).push(pagoEmbajador)
            transaccion.pagos.push(pagoEmbajador)
            pagarEmpresa(Paquetes[indexHijo].comisionNuevoIngreso, usuario, transaccion, "Comisión por concepto de nuevo registro")
            //se registran los pagos embajador
            let trans = await admin.database().ref("transacciones/").push(transaccion)
            transaccion.pagos.map(async (val)=>{
                val.transaccionID= trans.key
                await admin.database().ref("pagos/").push(val)
            })
            delete transaccion.pagos
            await admin.database().ref("transaccionesporcliente/").child(walletCliente).push(transaccion)
            await admin.database().ref("transaccionesporcliente/").child(walletPapa).push(transaccion)
            //se paga al hijo 
            resolve()
            
        })
        
    })
}

function guardarCliente(usuario, walletCliente, walletPapa) {
    return new Promise( async (resolve, reject)=>{
        console.log("ejecutando crear cliente")
        try {
            //guardamos en todos los clientes
            await admin.database().ref("usuarios").child(walletCliente).set(usuario)
            //traemos rutas del papa
            admin.database().ref("rutas").child(walletPapa).once("value", async (snap)=>{
                let rutas = snap.val()
                let url = "";
                let payload = null
                //si es hijo de un socio
                if(rutas.ruta === "raiz"){
                    //registramos como hijo del padre
                    url = "arbol"
                    payload = { ruta: [walletPapa] }
                //si es hijo de un embajador
                } else if(Array.isArray(rutas.ruta)) {
                    //se crea la ruta a el
                    let subdirectorio= ""
                    rutas.ruta.map(val=>{
                        subdirectorio+= val+"/hijos/"
                    })
                    url = "arbol/"+subdirectorio
                    payload = { ruta: rutas.ruta.concat([walletPapa]) }
                }
                await admin.database().ref(url).child(walletPapa).child("hijos").child(walletCliente).set(usuario)
                await admin.database().ref("rutas").child(walletCliente).set(payload)
                resolve()
            })
        } catch (error) {
            console.log("ocurrio un error", error)
            reject()
        }
    })
    
}

function repartirMensualidadPapas(transaccion, paqueteCliente, _usuario){
    return new Promise((resolve, reject)=>{
        let today = new Date()
        //guardamos la variable cpn lo no cobrado para enviar al final a la empresa
        console.log("vamos a repartir mensualidad papas")
        //procesamos el primer referido, que debe de tener
        admin.database().ref("rutas").child(_usuario.wallet).once("value", async (snap)=>{
            let rutas = snap.val()
            rutas = rutas.ruta
            console.log("se traheron las rutas", rutas)
            let arrayPapas = []
            console.log("se repartira nivel 1")
            //procesamos el ultimo que debe estar
            let ultimo = rutas[ rutas.length -1 ]
            arrayPapas.push(ultimo)
            await procesarPagoPapa(transaccion, ultimo,1,  _usuario, paqueteCliente.nivel1);
            //vemos si tiene un segundo
            if(rutas.length > 1){
                console.log("se repartira nivel 2")
                let penultimo = rutas[ rutas.length -2 ]
                arrayPapas.push(penultimo)
                await procesarPagoPapa(transaccion, penultimo,2,  _usuario, paqueteCliente.nivel2);
            } else {
                transaccion.pagos.push({
                    desde: _usuario,
                    finalizoen: {
                        nombre: "Empresa",
                        wallet : ""
                    },
                    monto : paqueteCliente.nivel2,
                    concepto: "Pago por falta de referenciador en renovación mensual",
                    detalles: "El pago era para referenciador nivel 2",
                    moneda: "ETH",
                    fechaRegistro: today.getTime()
                })
            }
            //si tiene un tercero
            if(rutas.length > 2){
                console.log("se repartira nivel 3")
                let antepenultimo = rutas[ rutas.length -3 ]
                arrayPapas.push(antepenultimo)
                await procesarPagoPapa(transaccion, antepenultimo,3,  _usuario, paqueteCliente.nivel3);
            } else {
                transaccion.pagos.push({
                    desde: _usuario,
                    finalizoen: {
                        nombre: "Empresa",
                        wallet : ""
                    },
                    monto : paqueteCliente.nivel3,
                    concepto: "Pago por falta de referenciador en renovación mensual",
                    detalles: "El pago era para referenciador nivel 3",
                    moneda: "ETH",
                    fechaRegistro: today.getTime()
                })
            }
            resolve(arrayPapas)
        })
    })
    
    
}
function procesarPagoPapa(transaccion, walletPapa, nivel, _usuario, comision) {
    return new Promise((resolve, reject)=>{
        console.log("venimos a procesar pago papa", walletPapa)
        admin.database().ref("usuarios").child(walletPapa).once("value", async (snap)=>{
            let papa = snap.val()
            //si el nivel del paquete comprado es mas alto que el que tiene el referenciador
            if(_usuario.indexPaqueteComprado > papa.indexPaqueteComprado){
                console.log("se pagara con limitaciones")
                let costoPaquetePapa = Paquetes[papa.indexPaqueteComprado][nivel == 1 ? "nivel1" : nivel== 2 ? "nivel2" : "nivel3"];
                let noPagadoPorLimites = comision - costoPaquetePapa;
                await pagarReferido(transaccion,nivel, papa, costoPaquetePapa, _usuario, true, noPagadoPorLimites);
            //si el paquete es menor o igual se paga todo
            } else {
                console.log("se pagara comision entera")
                await pagarReferido(transaccion, nivel, papa, comision, _usuario, false, 0);
            }
            resolve()
        })
    })
}
function pagarReferido(transaccion, nivel, _referenciador, _aPagar, _usuario, limitado, perdido){
    return new Promise(async (resolve, reject)=>{
        let today = new Date()
        console.log("se compara para pagar referido", _referenciador.fechaExpira, today.getTime())
        //si la cuenta del referenciador esta activa, le hacemos su pago
        let fechaExpira = new Date(_referenciador.fechaExpira)
        let aux
        if( fechaExpira > today || _referenciador.tipoCuenta=="Socio"){
            //limitaciones del paquete
            aux = {
                desde: _usuario,
                para: _referenciador,
                finalizoen: _referenciador,
                monto : _aPagar,
                concepto: "Pago por renovación de suscripción nivel "+nivel,
                detalles: limitado? "Limitado por paquete del referenciador, ganancias perdidas: "+ perdido+" ETH": "",
                moneda: "ETH",
                fechaRegistro: today.getTime()
            }
            console.log("la cuenta del referenciador si existe, se le pago", _referenciador)
        //si la cuenta del referenciador no esta activa, el pago se envia a la empresa
        } else {
            aux = {
                desde: _usuario,
                para: _referenciador,
                finalizoen: {
                    nombre: "Empresa",
                    wallet : ""
                },
                monto : _aPagar,
                concepto: "Redirección del pago por cuenta inactiva en renovación mensual nivel "+nivel,
                detalles: "El pago era para "+_referenciador.nombre+" ("+_referenciador.wallet+")",
                moneda: "ETH",
                fechaRegistro: today.getTime()
            }
            console.log("la cuenta del referenciador no existe o esta inactiva", _referenciador)
        }
        //se agrega el pago
        transaccion.pagos.push(aux)
        //se registra en el cliente 
        await admin.database().ref("pagosporcliente/").child(_referenciador.wallet).push(aux)
        if(limitado){
            transaccion.pagos.push({
                desde: _usuario,
                para: _referenciador,
                finalizoen: {
                    nombre: "Empresa",
                    wallet : ""
                },
                monto : perdido,
                concepto: "Pago por limitante de paquete referenciador en nivel "+nivel,
                detalles: "",
                moneda: "ETH",
                fechaRegistro: today.getTime()
            })
            console.log("se paga a la empresa porque no habia refer", _referenciador)
        }
        resolve()
    })
    
}

function cobrarComision(transaccion, remanente, _usuario) {
    let today = new Date()
    //si se enviara el 10 a regalos
    if(_usuario.indexPaqueteComprado > 3){
        console.log("se guardara el 10")
        transaccion.pagos.push({
            desde: _usuario,
            finalizoen: {
                nombre: "Regalos",
                wallet : ""
            },
            monto : remanente * .1,
            concepto: "Ahorro para regalos a los embajadores",
            detalles: "",
            moneda: "ETH",
            fechaRegistro: today.getTime()
        })
        pagarEmpresa((remanente * .2), _usuario, transaccion, "Comisión por concepto de reactivación")
    } else {
        pagarEmpresa((remanente * .3), _usuario, transaccion, "Comisión por concepto de reactivación")
    }
    
}

function pagarEmpresa(monto, _usuario, transaccion, detalles){
    console.log("pagar empresa ")
    let today = new Date()
    let porcEmpresa = monto * .10
    let porcSocios = monto * .45
    transaccion.pagos.push({
        desde: _usuario,
        finalizoen: {
            nombre: "Empresa",
            wallet : ""
        },
        monto : porcEmpresa,
        concepto: "Comision residual empresa",
        detalles,
        moneda: "ETH",
        fechaRegistro: today.getTime()
    })
    transaccion.pagos.push({
        desde: _usuario,
        finalizoen: {
            nombre: "Socio A",
            wallet : ""
        },
        monto : porcSocios,
        concepto: "Comision residual Socio A",
        detalles,
        moneda: "ETH",
        fechaRegistro: today.getTime()
    })
    transaccion.pagos.push({
        desde: _usuario,
        finalizoen: {
            nombre: "Socio B",
            wallet : ""
        },
        monto : porcSocios,
        concepto: "Comision residual Socio B",
        detalles,
        moneda: "ETH",
        fechaRegistro: today.getTime()
    })
}
