const { assert } = require("chai");

const Token = artifacts.require("Token");
const EthSwap = artifacts.require("EthSwap");

require("chai").use(require("chai-as-promised")).should()

function tokens(n) {
    return web3.utils.toWei(n, "ether")
}

contract("EthSwap", (accounts)=> {
    let token, ethSwap
    before(async()=>{
        token = await Token.new()
        ethSwap = await EthSwap.new(token.address)
        await token.transfer(ethSwap.address, "10000000000000000000000000")
    })

    describe("EthSwap deployment", async()=>{
        it("contract has a name", async ()=>{
            const name = await ethSwap.name()
            assert.equal(name, "EthSwap Instant Exchange")
        })
        it("contract has tokens", async()=>{
            let balance = await token.balanceOf(ethSwap.address)
            assert.equal(balance.toString(), "10000000000000000000000000")
        })
    })
    describe("Token deployment", async()=>{
        it("token has a name", async ()=>{
            const name = await token.name()
            assert.equal(name, "Midhas Gold")
        })
    })

    describe("buy tokens", async()=>{
        let result 
        before(async ()=>{
            result = await ethSwap.buyTokens({from: accounts[1], value: web3.utils.toWei("1", "ether")})
        })
        it("permite comprar tokens ", async ()=>{
            let investorBalance = await token.balanceOf(accounts[1])
            assert.equal(investorBalance.toString(), tokens(".2"))
        })
        it("el balance queda perfecto en ethswap", async()=>{
            let ethSwapBalance = await token.balanceOf(ethSwap.address)
            assert.equal(ethSwapBalance.toString(), tokens("9999999.8"))
        })
        it("el balance de ethswap es 1 eth", async()=>{
            let ethSwapBalance = await web3.eth.getBalance(ethSwap.address)
            assert.equal(ethSwapBalance.toString(), web3.utils.toWei("1", "ether"))
            console.log(result.logs)
        })
    })

    describe("sell tokens", async()=>{
        let result 
        before(async ()=>{
            await token.approve(ethSwap.address, tokens(".2"), {from:accounts[1]})
            result = await ethSwap.sellTokens(tokens(".2"), {from: accounts[1]})
        })

        it("permite vender tokens ", async ()=>{
            let investorBalance = await token.balanceOf(accounts[1])
            assert.equal(investorBalance.toString(), tokens("0"))
        })
        it("el balance queda entero en ethswap", async()=>{
            let ethSwapBalance = await token.balanceOf(ethSwap.address)
            assert.equal(ethSwapBalance.toString(), tokens("10000000"))
            console.log(result.logs)
        })

        it("investor no puede vender mas ", async()=>{
            await ethSwap.sellTokens(tokens("500"), {from: accounts[1]}).should.be.rejected;
        })
    })
})